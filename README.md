# CSV Histograms

This project draws csvs from histograms using Python 3.6 and pandas.

# Setup

1. Create a virtual environment
```
C:\Python36\python.exe -m venv venv
```

2. Activate the virtual environment
```
venv\Scripts\activate
```

3. Install the required packages 
```
python -m pip install -r requirements.txt
```

4. Run the jupyter notebook
```
jupyter notebook
```